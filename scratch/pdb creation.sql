-- READ : https://oracle-base.com/articles/12c/multitenant-create-and-configure-pluggable-database-12cr1

CREATE PLUGGABLE DATABASE pdbstaging ADMIN USER user_staging IDENTIFIED BY password CREATE_FILE_DEST='/opt/oracle/oradata/pdbstaging/';
CREATE PLUGGABLE DATABASE pdbwarehouse ADMIN USER user_warehouse IDENTIFIED BY password CREATE_FILE_DEST='/opt/oracle/oradata/pdbwarehouse/';
