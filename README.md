# Data Warehouse Final Exam

## Team 2 (7-12)

### Tasks 
- ETL (Extract, Transform, Load) concept explaination.
- Datawarehouse tuning concept explaination.
- Demo

### Target
- Show total selling data per product name per month per year
- Show total selling data per product name per quarter per year

## Architecture 
![architecture](art/diagram.png)
