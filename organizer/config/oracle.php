<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_ORACLE_MAIN_TNS', ''),
        'host'           => env('DB_ORACLE_MAIN_HOST', '34.142.177.201'),
        'port'           => env('DB_ORACLE_MAIN_PORT', '1521'),
        'database'       => env('DB_ORACLE_MAIN_DATABASE', ''),
        'service_name'   => env('DB_ORACLE_MAIN_SERVICE_NAME', 'ORCLPDB1'),
        'username'       => env('DB_ORACLE_MAIN_USERNAME', 'jack'),
        'password'       => env('DB_ORACLE_MAIN_PASSWORD', 'password'),
        'charset'        => env('DB_ORACLE_MAIN_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_ORACLE_MAIN_PREFIX', ''),
        'prefix_schema'  => env('DB_ORACLE_MAIN_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_ORACLE_MAIN_EDITION', 'ora$base'),
        'server_version' => env('DB_ORACLE_MAIN_SERVER_VERSION', '11g'),
        'load_balance'   => env('DB_ORACLE_MAIN_LOAD_BALANCE', 'yes'),
        'dynamic'        => [],
    ],
];
