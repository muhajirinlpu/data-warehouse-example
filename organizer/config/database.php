<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'oracle-trx' => [
            'driver'         => 'oracle',
            'tns'            => env('DB_ORACLE_MAIN_TNS', ''),
            'host'           => env('DB_ORACLE_MAIN_HOST', 'db.volt.kembang.in'),
            'port'           => env('DB_ORACLE_MAIN_PORT', '1521'),
            'database'       => env('DB_ORACLE_MAIN_DATABASE', ''),
            'service_name'   => env('DB_ORACLE_MAIN_SERVICE_NAME', 'ORCLPDB1'),
            'username'       => env('DB_ORACLE_MAIN_USERNAME', 'jack'),
            'password'       => env('DB_ORACLE_MAIN_PASSWORD', 'password'),
            'charset'        => env('DB_ORACLE_MAIN_CHARSET', 'AL32UTF8'),
            'prefix'         => env('DB_ORACLE_MAIN_PREFIX', ''),
            'prefix_schema'  => env('DB_ORACLE_MAIN_SCHEMA_PREFIX', ''),
            'edition'        => env('DB_ORACLE_MAIN_EDITION', 'ora$base'),
            'server_version' => env('DB_ORACLE_MAIN_SERVER_VERSION', '11g'),
            'load_balance'   => env('DB_ORACLE_MAIN_LOAD_BALANCE', 'yes'),
            'dynamic'        => [],
        ],

        'oracle-staging' => [
            'driver'         => 'oracle',
            'tns'            => env('DB_ORACLE_STAGING_TNS', ''),
            'host'           => env('DB_ORACLE_STAGING_HOST', 'db.volt.kembang.in'),
            'port'           => env('DB_ORACLE_STAGING_PORT', '1521'),
            'database'       => env('DB_ORACLE_STAGING_DATABASE', ''),
            'service_name'   => env('DB_ORACLE_STAGING_SERVICE_NAME', 'PDBSTAGING'),
            'username'       => env('DB_ORACLE_STAGING_USERNAME', 'user_staging'),
            'password'       => env('DB_ORACLE_STAGING_PASSWORD', 'password'),
            'charset'        => env('DB_ORACLE_STAGING_CHARSET', 'AL32UTF8'),
            'prefix'         => env('DB_ORACLE_STAGING_PREFIX', ''),
            'prefix_schema'  => env('DB_ORACLE_STAGING_SCHEMA_PREFIX', ''),
            'edition'        => env('DB_ORACLE_STAGING_EDITION', 'ora$base'),
            'server_version' => env('DB_ORACLE_STAGING_SERVER_VERSION', '11g'),
            'load_balance'   => env('DB_ORACLE_STAGING_LOAD_BALANCE', 'yes'),
            'dynamic'        => [],
        ],

        'oracle-warehouse' => [
            'driver'         => 'oracle',
            'tns'            => env('DB_ORACLE_MAIN_TNS', ''),
            'host'           => env('DB_ORACLE_MAIN_HOST', 'db.volt.kembang.in'),
            'port'           => env('DB_ORACLE_MAIN_PORT', '1521'),
            'database'       => env('DB_ORACLE_MAIN_DATABASE', ''),
            'service_name'   => env('DB_ORACLE_MAIN_SERVICE_NAME', 'PDBWAREHOUSE'),
            'username'       => env('DB_ORACLE_MAIN_USERNAME', 'user_warehouse'),
            'password'       => env('DB_ORACLE_MAIN_PASSWORD', 'password'),
            'charset'        => env('DB_ORACLE_MAIN_CHARSET', 'AL32UTF8'),
            'prefix'         => env('DB_ORACLE_MAIN_PREFIX', ''),
            'prefix_schema'  => env('DB_ORACLE_MAIN_SCHEMA_PREFIX', ''),
            'edition'        => env('DB_ORACLE_MAIN_EDITION', 'ora$base'),
            'server_version' => env('DB_ORACLE_MAIN_SERVER_VERSION', '11g'),
            'load_balance'   => env('DB_ORACLE_MAIN_LOAD_BALANCE', 'yes'),
            'dynamic'        => [],
        ],

        'mysql-trx' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_MYSQL_MAIN_HOST', 'db.volt.kembang.in'),
            'port' => env('DB_MYSQL_MAIN_PORT', '3306'),
            'database' => env('DB_MYSQL_MAIN_DATABASE', 'retail'),
            'username' => env('DB_MYSQL_MAIN_USERNAME', 'armageddon'),
            'password' => env('DB_MYSQL_MAIN_PASSWORD', 'secretVaultNeeded'),
            'unix_socket' => env('DB_MYSQL_MAIN_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'search_path' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            // 'encrypt' => env('DB_ENCRYPT', 'yes'),
            // 'trust_server_certificate' => env('DB_TRUST_SERVER_CERTIFICATE', 'false'),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'username' => env('REDIS_USERNAME'),
            'password' => env('REDIS_PASSWORD'),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'username' => env('REDIS_USERNAME'),
            'password' => env('REDIS_PASSWORD'),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];
