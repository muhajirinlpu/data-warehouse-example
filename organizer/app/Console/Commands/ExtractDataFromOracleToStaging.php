<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;

class ExtractDataFromOracleToStaging extends Command
{
    public const SOURCE_NAME = 'DB01';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dw:load:from-oracle {--product} {--sell}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract and Transform Transactional Oracle Data to Staging Data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        if ($this->option('product')) {
            $this->processProducts();
        } elseif ($this->option('sell')) {
            ini_set('memory_limit', '2G');
            $this->processSells();
        }

        $this->comment('data send to horizon.');

        return 0;
    }


    private function processProducts(): void
    {
        $this->info('start importing products (using horizon).');

        $products = DB::connection('oracle-trx')
            ->table('products')
            ->select(['prod_id', 'prod_name'])
            ->cursor();

        $bar = $this->output->createProgressBar($products->count());
        $bar->start();

        $jobs = $products->map(function ($product) use ($bar) {
            $bar->advance();

            return static function () use ($product) {
                DB::connection('oracle-staging')->table('products')->updateOrInsert([
                    'code' => $product->prod_id,
                    'name' => $product->prod_name,
                    'source' => self::SOURCE_NAME,
                ]);
            };
        });

        Bus::batch($jobs)->dispatch();

        $bar->finish();
        $this->getOutput()->newLine(2);
    }

    private function processSells(): void
    {
        $this->info('start importing sells data.');

        $productData = DB::connection('oracle-staging')
            ->table('products')
            ->where('source', self::SOURCE_NAME)
            ->cursor();

        $bar = $this->output->createProgressBar(DB::connection('oracle-trx')->table('sales')->count());
        $bar->start();

        $productData->each(function ($product) use ($bar) {
            $sells = DB::connection('oracle-trx')
                ->table('sales')
                ->where('prod_id', $product->code)
                ->cursor();

            $sells->each(function ($sell) use ($product, $bar) {
                $callback = static function () use ($sell, $product) {
                    $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $sell->time_id);
                    $identifier = $sell->prod_id . '-' . $sell->cust_id . '-' . $datetime->format('Y-m-d').'-'.$sell->quantity_sold.'-'.$sell->amount_sold;

                    DB::connection('oracle-staging')->table('sells')->updateOrInsert([
                        'identifier' => $identifier,
                    ],[
                        'product_id' => $product->id,
                        'bought_at' => (string)$datetime,
                        'pcs' => (int)$sell->quantity_sold,
                        'total_price' => (double)$sell->amount_sold,
                        'identifier' => $identifier,
                    ]);
                };

                dispatch($callback);

                $bar->advance();
            });
        });

        $bar->finish();
        $this->getOutput()->newLine(2);
    }
}
