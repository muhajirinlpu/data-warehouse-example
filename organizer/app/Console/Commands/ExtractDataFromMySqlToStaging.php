<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class ExtractDataFromMySqlToStaging extends Command
{
    public const SOURCE_NAME = 'DB02';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dw:load:from-mysql';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extract and Transform Transactional MySQL Data to Staging Data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->processProducts();
        $this->processSells();

        return 0;
    }

    private function processProducts(): void
    {
        $this->info('start importing products (using horizon).');

        $products = DB::connection('mysql-trx')
            ->table('t_barang')
            ->select(['id', 'nama'])
            ->cursor();

        $bar = $this->output->createProgressBar($products->count());
        $bar->start();

        $products->each(function ($product) use ($bar) {
            DB::connection('oracle-staging')->table('products')->updateOrInsert([
                'code' => $product->id,
                'name' => $product->nama,
                'source' => self::SOURCE_NAME,
            ]);

            $bar->advance();
        });

        $bar->finish();
        $this->getOutput()->newLine(2);
    }

    private function processSells(): void
    {
        $this->info('start importing sells data.');

        $productData = DB::connection('oracle-staging')->table('products')->where('source', self::SOURCE_NAME)->cursor();

        $bar = $this->output->createProgressBar(DB::connection('mysql-trx')->table('t_det_jual')->count());
        $bar->start();

        $productData->each(function ($product) use ($bar) {
            $sells = DB::connection('mysql-trx')
                ->table('t_det_jual')
                ->join('t_jual', function (JoinClause $clause) {
                    $clause->on('t_det_jual.nota', '=', 't_jual.nota');
                })
                ->where('id_brg', $product->code)
                ->select([
                    't_det_jual.jml_bsr as jml_bsr',
                    't_det_jual.jml_kcl as jml_kcl',
                    't_det_jual.total as total',
                    't_det_jual.id_brg as id_brg',
                    't_det_jual.nota as nota',
                    't_jual.tgl_nota as tgl_nota',
                ])
                ->cursor();

            $sells->each(static function ($sell) use ($product, $bar) {
                dispatch(static function () use ($sell, $product) {
                    DB::connection('oracle-staging')->table('sells')->updateOrInsert([
                        'identifier' => $sell->nota . '-' . $sell->id_brg,
                    ],[
                        'product_id' => $product->id,
                        'bought_at' => (string)Carbon::createFromFormat('Y-m-d', $sell->tgl_nota),
                        'pcs' => (int)$sell->jml_bsr + (int)$sell->jml_kcl,
                        'total_price' => (double)$sell->total,
                        'identifier' => $sell->nota . '-' . $sell->id_brg,
                    ]);

                });
                $bar->advance();
            });
        });

        $bar->finish();
        $this->getOutput()->newLine(2);
    }
}
