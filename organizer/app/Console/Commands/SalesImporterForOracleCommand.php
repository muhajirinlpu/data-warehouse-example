<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Support\Facades\DB;

class SalesImporterForOracleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importer:oracle-csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Async import csv data sales into oracle db.';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \League\Csv\Exception
     * @throws \League\Csv\InvalidArgument
     */
    public function handle(): int
    {
        ini_set("memory_limit", "2G");

        $filePath = base_path('../storage/dump/oracle/sh_sales.csv');

        $csv = Reader::createFromPath($filePath);
        $csv->setDelimiter(';');
        $stmt = Statement::create();

        $records = $stmt->process($csv);

        $data = collect($records)
            ->filter(static fn ($columns) => !(count($columns) < 5))
            ->map(static fn ($columns) => [
                'PROD_ID' => $columns[0],
                'CUST_ID' => $columns[1],
                'TIME_ID' => (string) Carbon::createFromFormat("d-M-Y", $columns[2]),
                'QUANTITY_SOLD' => $columns[3],
                'AMOUNT_SOLD' => $columns[4],
            ]);


        $bar = $this->output->createProgressBar($data->count());
        $bar->start();

        $data->each(function ($datum) use ($bar) {
            dispatch(static function () use ($datum) {
                DB::connection('oracle-trx')->table('sales')->insert($datum);
            });

            $bar->advance();
        });

        $bar->finish();

        return 0;
    }
}
