<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class BuildWarehouseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dw:build:warehouse ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Staging Data into Data Warehouse.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $query = DB::connection('oracle-staging')
            ->table('sells')
            ->join('products', static function (JoinClause $clause) {
                $clause->on('products.id', '=', 'sells.product_id');
            })
            ->select(['sells.*', 'products.name']);

        $bar = $this->output->createProgressBar($query->count());
        $bar->start();

        $data = $query->cursor();

        $data->each(static function ($datum) use ($bar) {
            dispatch(static function () use ($datum) {
                $identifier = $datum->identifier;
                $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $datum->bought_at);

                DB::connection('oracle-warehouse')->table('sells')->updateOrInsert(
                    [
                        'IDENTIFIER' => $identifier,
                    ],
                    [
                        'YEAR' => $dateTime->year,
                        'QUART' => $dateTime->quarter,
                        'MONTH' => $dateTime->month,
                        'PRODUCT_NAME' => $datum->name,
                        'AMOUNT' => $datum->pcs,
                        'IDENTIFIER' => $identifier,
                    ]
                );
            });

            $bar->advance();
        });

        return 0;
    }
}
